from ctypes import resize
import numpy as np
import cv2
from meenkando.GeneratePlots import PlotFunctions
from meenkando.TimeStamp import timestamp
from PIL import Image
from skimage import morphology, img_as_bool
import collections
import networkx as nx
from meenkando.utils import ProgressBar
from meenkando.FishSpline import FishSkelton
from meenkando.utils import AnglesCalc
from numpy import cumsum, dtype
from meenkando.utils import FindPeaks


class Vertex:
    def __init__(self, point, degree=0, edges=None):
        self.point = np.asarray(point)
        self.degree = degree
        self.edges = []
        self.visited = False
        if edges is not None:
            self.edges = edges

    def __str__(self):
        return str(self.point)


class Edge:

    def __init__(self, start, end=None, pixels=None):
        self.start = start
        self.end = end
        self.pixels = []
        if pixels is not None:
            self.pixels = pixels
        self.visited = False


def buildTree(img, start=None):

    # copy image since we set visited pixels to black
    img = img.copy()
    shape = img.shape
    nWhitePixels = np.sum(img)

    # neighbor offsets (8 nbors)
    nbPxOff = np.array([[-1, -1], [-1, 0], [-1, 1],
                        [0, -1],          [0, 1],
                        [1, -1], [1, 0], [1, 1]
                        ])

    queue = collections.deque()

    # a list of all graphs extracted from the skeleton
    graphs = []

    blackedPixels = 0
    # we build our graph as long as we have not blacked all white pixels!
    while nWhitePixels != blackedPixels:

        # if start not given: determine the first white pixel
        if start is None:
            it = np.nditer(img, flags=['multi_index'])
            while not it[0]:
                it.iternext()

            start = it.multi_index

        startV = Vertex(start)
        queue.append(startV)
        #print("Start vertex: ", startV)

        # set start pixel to False (visited)
        img[startV.point[0], startV.point[1]] = False
        blackedPixels += 1

        # create a new graph
        G = nx.Graph()
        G.add_node(startV)

        # build graph in a breath-first manner by adding
        # new nodes to the right and popping handled nodes to the left in queue
        while len(queue):
            currV = queue[0]  # get current vertex

            # check all neigboor pixels
            for nbOff in nbPxOff:

                # pixel index
                pxIdx = currV.point + nbOff

                if (pxIdx[0] < 0 or pxIdx[0] >= shape[0]) or (pxIdx[1] < 0 or pxIdx[1] >= shape[1]):
                    continue  # current neigbor pixel out of image

                if img[pxIdx[0], pxIdx[1]]:
                    #print( "nb: ", pxIdx, " white ")
                    # pixel is white
                    newV = Vertex([pxIdx[0], pxIdx[1]])

                    # add edge from currV <-> newV
                    G.add_edge(currV, newV, object=Edge(currV, newV))
                    # G.add_edge(newV,currV)

                    # add node newV
                    G.add_node(newV)

                    # push vertex to queue
                    queue.append(newV)

                    # set neighbor pixel to black
                    img[pxIdx[0], pxIdx[1]] = False
                    blackedPixels += 1

            # pop currV
            queue.popleft()
        # end while

        # empty queue
        # current graph is finished ->store it
        graphs.append(G)

        # reset start
        start = None

    # end while

    return graphs, img


def getEndNodes(g):
    return [n for n in nx.nodes(g) if nx.degree(g, n) == 1]


def create_connected_graph(_coords):
    g_new = nx.Graph()
    firstV = Vertex(_coords[0])
    for idx in range(1, _coords.shape[0]):
        if idx == 1:
            currV = firstV
        newV = Vertex(_coords[idx])
        g_new.add_node(newV)
        g_new.add_edge(currV, newV, object=Edge(currV, newV))
        currV = newV
    return g_new


def cal_head_line(head_skelton):

    y = np.where(head_skelton > 0)[0]
    x = np.where(head_skelton > 0)[1]
    sort_idx = x.argsort()
    x = x[sort_idx]
    y = y[sort_idx]
    xx, x_count = np.unique(x, return_counts=True)
    split_y = np.split(y, cumsum(x_count))
    return [xx[:5].mean(), np.asarray([arr.mean() for arr in np.asarray(split_y[:5], dtype='object')], dtype='object').mean()]


def extract_fish_splines(img_file, low_thresh, high_thresh, tail_portion=40, return_images=False):
    img = cv2.imread(img_file)
    gray_fish = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    im = cv2.fastNlMeansDenoising(gray_fish, None, 6, 1, 3)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 2))
    closing = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)
    resized = cv2.resize(closing, (128, 128), interpolation=cv2.INTER_AREA)

    fish_skelton = FishSkelton.extract_skelton(
        resized, 5, low_thresh, high_thresh)
    head_skelton = FishSkelton.extract_skelton(resized, 5, 250, 265)
    head_point = cal_head_line(head_skelton)

    img_fish = Image.fromarray(fish_skelton, 'L')
    bool_img = img_as_bool(img_fish)
    # do some closing (noise removing)
    d = morphology.disk(10)
    bool_img = morphology.binary_closing(bool_img, selem=d)

    # do the skeletonization
    imgSk = morphology.medial_axis(bool_img)
    fish_tail = imgSk[:, tail_portion:]

    # Find a start pixel (not necessary)
    y = 0
    x = np.where(fish_tail[:, y] == True)[0][0]

    # disconnect
    imgSkT = fish_tail.copy()
    # imgSkT[59,117] = False

    graphs, imgB = buildTree(imgSkT, np.array([x, y]))
    g1 = graphs[0]
    nodes_list_1 = g1.nodes
    edges_list = g1.edges

    path_coords = []
    for node in nodes_list_1:
        path_coords.append(node.point)

    tail_coords = np.vstack(path_coords)
    tail_coords[:, 1] += tail_portion

    g2 = create_connected_graph(tail_coords)
    end_nodes = getEndNodes(g2)
    shrts_path = nx.shortest_path(g2, end_nodes[0], end_nodes[1])

    nodes_list_2 = list(g2.nodes)

    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

    spline_coords = []
    for node in nodes_list_2[0::4]:
        spline_coords.append(node.point)

    if return_images:
        return gray_fish, fish_skelton, bool_img, g2, nodes_list_2
    return resized, bool_img.astype(int), np.stack(spline_coords), head_point


def create_fish_film(dir_path, imgfiles, time_, low_thres, high_thres, tap_idxs, des_str):

    start_idx, end_idx = timestamp.find_tap_positions(tap_idxs)

    for ii in range(len(start_idx)):
        Imgfiles = np.asarray([dir_path+imgfile for
                               imgfile in
                               imgfiles[start_idx[ii]-50:end_idx[ii]+200]])

        for imgfile in ProgressBar.log_progress(Imgfiles, every=1):
            imgfilename = imgfile.replace('.BMP', '')
            imgfilename = imgfilename.replace('.', '_')
            idx_no = int(imgfilename.rsplit('_')[-1])
            gray_fish, skelton_fish, bool_fish, graph2_tail, nd2_tail = extract_fish_splines(imgfile,
                                                                                             low_thres, high_thres,
                                                                                             return_images=True)

            PlotFunctions.create_fish_film_plots(time_[idx_no], idx_no, gray_fish, skelton_fish,
                                                 bool_fish, graph2_tail, nd2_tail, tap_idxs[idx_no], des_str)

    return None


def calculate_latency_with_plots(dir_path, time_, tap_idxs, imgfiles, cutoff_fac, first_mov,
                                 low_thres, high_thres, des_str, interp):
    start_idx, end_idx = timestamp.find_tap_positions(tap_idxs)
    for ii in range(len(start_idx)):
        try:
            Imgfiles = np.asarray([dir_path+imgfile for
                                   imgfile in
                                   imgfiles[start_idx[ii]-100:end_idx[ii]+300]])
        except:
            continue
        if Imgfiles.shape[0] < 300:
            continue
        time_idx = []
        x_points = []
        y_points = []
        head_points = []
        angles_ = []
        angles_all = []
        # try:
        for imgfile in ProgressBar.log_progress(Imgfiles, every=1):
            imgfilename = imgfile.replace('.BMP', '')
            imgfilename = imgfilename.replace('.', '_')
            idx_no = int(imgfilename.rsplit('_')[-1])
            spline_coordinates_of_fish, head_of_fish = extract_fish_splines(imgfile,
                                                                            low_thres, high_thres)
            if spline_coordinates_of_fish.shape[0] < 20:
                continue
            time_idx.append(time_[idx_no])
            start_row = (spline_coordinates_of_fish.shape[0] - 20) // 2
            x_ = spline_coordinates_of_fish[start_row:start_row+20, 1]
            y_ = spline_coordinates_of_fish[start_row:start_row+20, 0]
            head_points.append(head_of_fish)
            x_points.append(x_)
            y_points.append(y_)
        corr_fac = np.asarray(head_points) - np.asarray(head_points)[0, :]
        x_points = np.asarray(x_points) - corr_fac[:, 0].reshape(-1, 1)
        y_points = np.asarray(y_points) - corr_fac[:, 1].reshape(-1, 1)
        head_points = np.asarray(head_points) - corr_fac

        for row in range(head_points.shape[0]):
            angles_.append(AnglesCalc.ang_calc(
                x_points[row, :], y_points[row, :], head_points[row, :]))
            angles_all.append(AnglesCalc.ang_calc_all(
                x_points[row, :], y_points[row, :], head_points[row, :]))
        time_idx = np.asarray(time_idx)
        angles_ = np.asarray(angles_)
        angles_all = np.asarray(angles_all)
        PlotFunctions.plot_latency_interp_all(
            time_[start_idx[ii]], angles_all, time_idx)

        if angles_.shape[0] < 300:
            print('Not enough data to plot')
            continue
        if interp:
            PlotFunctions.calc_plot_latency_interp(time_[start_idx[ii]], time_[end_idx[ii]], angles_,
                                                   cutoff_fac, time_idx, ii, des_str, first_mov)
            # c_latency,bend_max,first_mov_lat = calc_latency_interp_bp(time_[start_idx[ii]],time_[end_idx[ii]],angles_all,
            # cutoff_fac,time_idx,ii,des_str,first_mov)
        elif not interp:
            PlotFunctions.calc_plot_latency(time_[start_idx[ii]], time_[end_idx[ii]], angles_,
                                            cutoff_fac, time_idx, ii, des_str, first_mov)

        # except:
            #print('Error detected in this tap. Will skip')
    return None


def calculate_latency_with_tabs(dir_path, time_, tap_idxs, imgfiles, cutoff_fac, first_mov,
                                low_thres, high_thres, des_str, interp):
    start_idx, end_idx = timestamp.find_tap_positions(tap_idxs)
    latencies_for_fish = []
    latencies_for_fish_all = []
    time_vs_stats = np.ones((1, 87))
    for ii in range(len(start_idx)):
        try:
            Imgfiles = np.asarray([dir_path+imgfile for
                                   imgfile in
                                   imgfiles[start_idx[ii]-100:end_idx[ii]+300]])
        except:
            continue
        if Imgfiles.shape[0] < 300:
            continue
        time_idx = []
        x_points = []
        y_points = []
        head_points = []
        angles_ = []
        angles_all = []
        try:
            for imgfile in ProgressBar.log_progress(Imgfiles, every=1):
                imgfilename = imgfile.replace('.BMP', '')
                imgfilename = imgfilename.replace('.', '_')
                idx_no = int(imgfilename.rsplit('_')[-1])
                spline_coordinates_of_fish, head_of_fish = extract_fish_splines(imgfile,
                                                                                low_thres, high_thres)
                if spline_coordinates_of_fish.shape[0] < 20:
                    continue
                time_idx.append(time_[idx_no])
                start_row = (spline_coordinates_of_fish.shape[0] - 20) // 2
                x_ = spline_coordinates_of_fish[start_row:start_row+20, 1]
                y_ = spline_coordinates_of_fish[start_row:start_row+20, 0]
                head_points.append(head_of_fish)
                x_points.append(x_)
                y_points.append(y_)
            corr_fac = np.asarray(head_points) - np.asarray(head_points)[0, :]
            x_points = np.asarray(x_points) - corr_fac[:, 0].reshape(-1, 1)
            y_points = np.asarray(y_points) - corr_fac[:, 1].reshape(-1, 1)
            head_points = np.asarray(head_points) - corr_fac

            for row in range(head_points.shape[0]):
                angles_.append(AnglesCalc.ang_calc(
                    x_points[row, :], y_points[row, :], head_points[row, :]))
                angles_all.append(AnglesCalc.ang_calc_all(
                    x_points[row, :], y_points[row, :], head_points[row, :]))
            time_idx = np.asarray(time_idx)
            angles_ = np.asarray(angles_)
            angles_all = np.asarray(angles_all)

            time_vs_stats = np.vstack((time_vs_stats,
                                       np.hstack((np.reshape(time_idx, (-1, 1)),
                                                  np.ones(
                                                      (len(time_idx), 1))*time_[start_idx[ii]],
                                                  np.ones(
                                                      (len(time_idx), 1))*[ii+1],
                                                  head_points, x_points, y_points,
                                                  angles_, angles_all))))

            if angles_.shape[0] < 300:
                print('Not enough data to calculate latency')
                continue

            c_latency, bend_max, first_mov_lat, _ = calc_latency_interp_bp(time_[start_idx[ii]], time_[end_idx[ii]], angles_,
                                                                           cutoff_fac, time_idx, ii, des_str, first_mov)
            c_latency_all, bend_max_all, first_mov_lat_all, peak_angs = calc_latency_interp_bp(time_[start_idx[ii]],
                                                                                               time_[
                                                                                                   end_idx[ii]],
                                                                                               angles_all, cutoff_fac,
                                                                                               time_idx, ii, des_str, first_mov)
            latencies_for_fish.append([ii+1]+c_latency+bend_max+first_mov_lat)
            latencies_for_fish_all.append(
                [ii+1]+c_latency_all+bend_max_all+first_mov_lat_all+peak_angs)
        except:
            print('Error detected in this tap. Skipping')
            latencies_for_fish.append([ii+1]+[-999]*9)
            latencies_for_fish_all.append([ii+1]+[-999]*156)

    return latencies_for_fish, latencies_for_fish_all, time_vs_stats


def create_fish_film_with_ang(dir_path, imgfiles, time_, low_thres, high_thres, tap_idxs, des_str):

    start_idx, end_idx = timestamp.find_tap_positions(tap_idxs)

    for ii in range(len(start_idx)):
        Imgfiles = np.asarray([dir_path+imgfile for
                               imgfile in
                               imgfiles[start_idx[ii]-50:end_idx[ii]+200]])

        x_points = []
        y_points = []
        head_points = []
        angles_ = []
        _, head_ref_point = extract_fish_splines(Imgfiles[0], 250, 265)
        for imgfile in ProgressBar.log_progress(Imgfiles, every=1):
            imgfilename = imgfile.replace('.BMP', '')
            imgfilename = imgfilename.replace('.', '_')
            idx_no = int(imgfilename.rsplit('_')[-1])
            gray_fish, skelton_fish, bool_fish, graph2_tail, nd2_tail = extract_fish_splines(imgfile,
                                                                                             low_thres, high_thres,
                                                                                             return_images=True)
            spline_coordinates_of_fish, head_of_fish = extract_fish_splines(imgfile,
                                                                            low_thres, high_thres)

            if spline_coordinates_of_fish.shape[0] < 20:
                continue
            start_row = (spline_coordinates_of_fish.shape[0] - 20) // 2
            x_ = spline_coordinates_of_fish[start_row:start_row+20, 1]
            y_ = spline_coordinates_of_fish[start_row:start_row+20, 0]

            corr_fac = np.asarray(head_of_fish) - head_ref_point
            x_ = np.asarray(x_) - corr_fac[0]
            y_ = np.asarray(y_) - corr_fac[1]
            head_of_fish = np.asarray(head_of_fish) - corr_fac

            angle_ = AnglesCalc.ang_calc(x_, y_, head_of_fish)

            PlotFunctions.create_fish_film_plots_with_ang(time_[idx_no], idx_no, gray_fish, skelton_fish,
                                                          bool_fish, graph2_tail, nd2_tail,
                                                          tap_idxs[idx_no], x_, y_, head_of_fish,
                                                          angle_, des_str)

    return None


def calc_latency_interp_bp(start_, end_, angles_, cutoff_fac, time_, tap_no, de_str, first_mov=3.5):

    tap_mark = start_
    marker_line = np.linspace(0, 200, 100)
    tap_time = 100 * [tap_mark]
    tap_time_idx = np.where(time_ >= tap_mark)[0][0]
    angles_abs = np.copy(angles_)
    angles_abs[0:tap_time_idx, :] = np.mean(angles_[0:tap_time_idx, :], axis=0)
    first_mov_latencies = []
    c_bend_latencies = []
    c_bend_max_latencies = []
    peak_angles = []
    for angle_id in range(angles_.shape[1]):
        pre_level_high = np.max(angles_abs[0:tap_time_idx, angle_id])
        pre_level_low = np.min(angles_abs[0:tap_time_idx, angle_id])

        new_time = np.linspace(time_[tap_time_idx], time_[
                               tap_time_idx+150], 1000)
        angles_interp_abs = np.interp(new_time, time_[tap_time_idx:tap_time_idx+140],
                                      angles_abs[tap_time_idx:tap_time_idx+140, angle_id])

        peak_idxs = FindPeaks.detect_peaks(
            angles_interp_abs, mph=pre_level_high*4, show=False)
        if len(peak_idxs) == 0:
            c_bend_max_latency = -999
            peak_angle = -999
        else:
            c_bend_max_latency = new_time[peak_idxs[0]]-tap_mark
            peak_angle = angles_interp_abs[peak_idxs[0]]
        c_bend_max_latencies.append(c_bend_max_latency)
        peak_angles.append(peak_angle)

        try:
            cutoff_high = np.where(angles_interp_abs >= (
                pre_level_high+cutoff_fac))[0][0]
        except:
            cutoff_high = 350

        c_bend_latencies.append(new_time[cutoff_high]-tap_mark)
        try:
            first_mov_high = np.where(
                angles_interp_abs >= (pre_level_high+first_mov))[0][0]
        except:
            first_mov_high = -999
        first_mov_latencies.append(new_time[first_mov_high]-tap_mark)

    return c_bend_latencies, c_bend_max_latencies, first_mov_latencies, peak_angles
