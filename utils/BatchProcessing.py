import os
import pandas as pd
import numpy as np
from meenkando.TimeStamp import timestamp
from meenkando.FilePicker import filepicker
from meenkando.GeneratePlots import PlotFunctions
from meenkando.FishSpline import FishSkelton
from meenkando.NetworkPath import SplineNetwork
import seaborn as sns

import warnings
warnings.filterwarnings('once')

def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        if len(files) > 0:
            print('{}{} files'.format(subindent, len(files)))
            
            
            
def bring_it_on(startpath,first_movement_cutoff=3.5,cutoff_factor=60,gen_tables=False):
    
    thirtytwoBit_table = timestamp.generate_bitTable()

        
    date_dirs = os.listdir(startpath)
    
    if not gen_tables:
        for _,date_dir in enumerate(date_dirs):
            category_dirs = os.listdir(startpath+'/'+date_dir)
            for _,cat_dir in enumerate(category_dirs):
                fish_dirs = os.listdir(startpath+'/'+date_dir+'/'+cat_dir)

                for _,fish_dir in enumerate(fish_dirs):

                    data_dir_path = startpath+'/'+date_dir+'/'+cat_dir+'/'
                    fish_path = fish_dir+'/'
                    desc_str = date_dir+' '+cat_dir+' '+fish_dir+' '
                    fish_files_sorted = filepicker.extract_files(data_dir_path,
                                                                 fish_path,
                                                                 file_type='.BMP')

                    time_stamps, tap_pixel_idx, missing_frames = timestamp.extract_timestamp(data_dir_path+fish_path,
                                                            fish_files_sorted,
                                                            thirtytwoBit_table,plots=True)
                    low_threshold_=20
                    high_threshold_=245

                    SplineNetwork.calculate_latency_with_plots(data_dir_path+fish_path,time_stamps,
                                    tap_pixel_idx,fish_files_sorted,cutoff_factor,first_movement_cutoff,
                                   low_threshold_,high_threshold_,desc_str,interp=True)
                    
            return None
    if gen_tables:
        
            for _,date_dir in enumerate(date_dirs):
                dict_latencies={date_dir:{}}
                dict_latencies_all={date_dir:{}}
                category_dirs = os.listdir(startpath+'/'+date_dir)
                for _,cat_dir in enumerate(category_dirs):
                    fish_dirs = os.listdir(startpath+'/'+date_dir+'/'+cat_dir)
                    print('#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@')
                    for _,fish_dir in enumerate(fish_dirs):
                        print('****************************************')
                        data_dir_path = startpath+'/'+date_dir+'/'+cat_dir+'/'
                        fish_path = fish_dir+'/'
                        desc_str = date_dir+' '+cat_dir+' '+fish_dir+' '
                        fish_files_sorted = filepicker.extract_files(data_dir_path,
                                                                     fish_path,
                                                                     file_type='.BMP')

                        time_stamps, tap_pixel_idx, missing_frames = timestamp.extract_timestamp(data_dir_path+fish_path,
                                                                fish_files_sorted,
                                                                thirtytwoBit_table,plots=False)
                        low_threshold_=5
                        high_threshold_=245

                        latencies,latencies_all,timeVSstats = SplineNetwork.calculate_latency_with_tabs(data_dir_path+
                                                                                                        fish_path,
                                                                                                       time_stamps,
                                                                                                       tap_pixel_idx,
                                                                                                   fish_files_sorted,
                                                                                                   cutoff_factor,
                                                                                                   first_movement_cutoff,
                                                                                            low_threshold_,high_threshold_,
                                                                                            desc_str,interp=True)
                        dict_latencies[date_dir][cat_dir+fish_dir] = latencies
                        dict_latencies_all[date_dir][cat_dir+fish_dir] = latencies_all
                        np.save(data_dir_path+cat_dir+'_fish_'+fish_dir+'_timeVSstats',timeVSstats)
                        
                for x in list(dict_latencies[date_dir].keys()):
                    if dict_latencies[date_dir][x] == []:
                        del dict_latencies[date_dir][x]
                        
                for x in list(dict_latencies_all[date_dir].keys()):
                    if dict_latencies_all[date_dir][x] == []:
                        del dict_latencies_all[date_dir][x]
                        
                final_list = []
                fish_names = []
                for x in list(dict_latencies[date_dir].keys()):
                    final_list.append(np.asarray(dict_latencies[date_dir][x]))
                    fish_names = fish_names + [x]*np.asarray(dict_latencies[date_dir][x]).shape[0]
                final_list = np.vstack(final_list)   
                final_df = pd.DataFrame(np.column_stack([fish_names,final_list]),
                                        columns=['fish_name','tap_no','C_h','C_ht','C_bt',
                                                 'Cmax_h','Cmax_ht','Cmax_bt','h','ht','bt'])
                final_df['Fish_group'] = final_df['fish_name'].map(lambda x: x.translate({ord(ch): None for 
                                                                                          ch in '0123456789'}))
                final_df.to_excel(date_dir+'.xlsx')
                
                
                final_list_all = []
                fish_names_all = []
                for x in list(dict_latencies_all[date_dir].keys()):
                    final_list_all.append(np.asarray(dict_latencies_all[date_dir][x]))
                    fish_names_all = fish_names_all + [x]*np.asarray(dict_latencies_all[date_dir][x]).shape[0]
                final_list_all = np.vstack(final_list_all)   
                final_df_all = pd.DataFrame(np.column_stack([fish_names_all,final_list_all]))
                gen_col_name = ['C_bend','C_max','first_mov','peak_ang']
                col_names = []
                for jj in range(4):
                    for ii in range(39):
                        col_names.append(gen_col_name[jj]+'_'+str(ii+1))
                
                final_df_all.columns = ['fish_name']+['tap_no']+col_names
                
                final_df_all['Fish_group'] = final_df_all['fish_name'].map(lambda x: x.translate({ord(ch): None for 
                                                                                          ch in '0123456789'}))
                final_df_all.to_excel(date_dir+'_sensory_overload.xlsx')
                
            return final_df

