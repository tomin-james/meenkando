import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools

def draw_boxplots(col_name,df_):
    arr = []
    arr.append(df_[df_['Fish_group']=='A'][col_name].values.astype('float64')*1000)
    arr.append(df_[df_['Fish_group']=='B'][col_name].values.astype('float64')*1000)
    arr.append(df_[df_['Fish_group']=='C'][col_name].values.astype('float64')*1000)
    plt.boxplot(arr)
    plt.ylim([-5,50])
    plt.show()
    
    return arr

def process_df(df,upper_cut=0.075,lower_cut=0.001):
    data_cols = [x for x in list(df.columns) if  any(c.isdigit() for c in x)]
    df[data_cols] = df[data_cols].apply(pd.to_numeric, errors='coerce')
    df[(df[data_cols]<=0.0) | (df[data_cols]>=upper_cut) | (df[data_cols]<=lower_cut) ] = np.nan
    
    return df,data_cols

def export_final_latency_results(latency_all_angle,ang_df,numeric_cols,category_names,output_str,
                                 median_stat=True,mean_stat=True):
    first_mov_arr = []
    latency_arr = []
    latency_max_arr = []
    ang_max_arr = []

    assert(median_stat ^ mean_stat)
    start_col = 0
    
    if median_stat:
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[0]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].median(axis=1).values*1000)
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[1]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].median(axis=1).values*1000)
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[2]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].median(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[0]]
                                [numeric_cols[start_col:start_col+39]].median(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[1]]
                                [numeric_cols[start_col:start_col+39]].median(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[2]]
                                [numeric_cols[start_col:start_col+39]].median(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[0]]
                                [numeric_cols[start_col+39:start_col+39*2]].median(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[1]]
                                [numeric_cols[start_col+39:start_col+39*2]].median(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[2]]
                                [numeric_cols[start_col+39:start_col+39*2]].median(axis=1).values*1000)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[0]]
                                [numeric_cols[start_col+39*3:]].median(axis=1).values)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[1]]
                                [numeric_cols[start_col+39*3:]].median(axis=1).values)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[2]]
                                [numeric_cols[start_col+39*3:]].median(axis=1).values)
    if mean_stat:
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[0]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].mean(axis=1).values*1000)
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[1]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].mean(axis=1).values*1000)
        first_mov_arr.append(latency_all_angle[latency_all_angle['Fish_group']== category_names[2]]
                                [numeric_cols[start_col+39*2:start_col+39*3]].mean(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[0]]
                                [numeric_cols[start_col:start_col+39]].mean(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[1]]
                                [numeric_cols[start_col:start_col+39]].mean(axis=1).values*1000)
        latency_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[2]]
                                [numeric_cols[start_col:start_col+39]].mean(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[0]]
                                [numeric_cols[start_col+39:start_col+39*2]].mean(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[1]]
                                [numeric_cols[start_col+39:start_col+39*2]].mean(axis=1).values*1000)
        latency_max_arr.append(latency_all_angle[latency_all_angle['Fish_group']==category_names[2]]
                                [numeric_cols[start_col+39:start_col+39*2]].mean(axis=1).values*1000)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[0]]
                                [numeric_cols[start_col+39*3:]].mean(axis=1).values)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[1]]
                                [numeric_cols[start_col+39*3:]].mean(axis=1).values)
        ang_max_arr.append(ang_df[ang_df['Fish_group']==category_names[2]]
                                [numeric_cols[start_col+39*3:]].mean(axis=1).values)

    final_results_df = pd.concat([pd.DataFrame(first_mov_arr).transpose(),
                                  pd.DataFrame(latency_arr).transpose(),
                                  pd.DataFrame(latency_max_arr).transpose(),
                                  pd.DataFrame(ang_max_arr).transpose()],axis=1)
    final_results_df.columns = list(itertools.chain.from_iterable([['first_mov '+category for category in category_names],
                                ['c_bend '+category for category in category_names],
                                ['c_bend_max '+category for category in category_names],
                                ['ang_max '+category for category in category_names]]))

                                  
    final_results_df.to_excel(output_str+'.xlsx')
