import math
import numpy as np


class Point():
    def __init__(self, x, y):
        self.x, self.y = x, y
    def __repr__(self):
        return '(%s, %s)'%(self.x, self.y)
    
    
def dot(v, u):
    return v[0]*u[0]+v[1]*u[1]

def ang(line_):
    # Get nicer vector form
    [x1,x2,y1,y2,x3,x4,y3,y4] = line_
    v1 = [(x1-x2), (y1-y2)]
    v2 = [(x3-x4), (y3-y4)]
    
    #v1 = [(line1[0][0]-line1[0][1]), (line1[1][0]-line1[1][1])]
    #v2 = [(line2[0][0]-line2[0][1]), (line2[1][0]-line2[1][1])]
    
    # Get dot prod
    dot_prod12 = np.dot(v1, v2)

    
    # Get magnitudes
    mag1 = np.dot(v1, v1)**0.5
    mag2 = np.dot(v2, v2)**0.5

    # Get cosine value
    cos_12 = dot_prod12/mag1/mag2
    

    # Get angle in radians and then convert to degrees
    angle_12 = math.acos(np.around(cos_12,decimals=4))

    # Basically doing angle <- angle mod 360
    ang_deg_12 = math.degrees(angle_12)         

    return ang_deg_12

def ang_calc(x,y,head_):
    # Get nicer vector form
    #line_head=[[x[0:4].mean(),x[4:7].mean()], [y[0:4].mean(),y[4:7].mean()]]


    line_head= [head_[0],x[0],head_[1],y[0]]
    line_mid=[x[7:9].mean(),x[10:12].mean(), y[7:9].mean(),y[10:12].mean()]
    line_tail=[x[16:18].mean(),x[18:20].mean(), y[16:18].mean(),y[18:20].mean()]
    
    #line_head = np.around(line_head, decimals=1)
    #line_mid = np.around(line_mid, decimals=1) 
    #line_tail = np.around(line_tail, decimals=1)
    
    ang_head_mid = ang(line_head+line_mid)
    ang_head_tail = ang(line_head+line_tail)
    ang_mid_tail = ang(line_mid+line_tail)
        
    return ang_head_mid,ang_head_tail,ang_mid_tail

def ang_calc_all(x,y,head_):
    # Get nicer vector form
    head_ = head_.astype(int)
    
    x = np.insert(x,0,head_[0])
    y = np.insert(y,0,head_[1])
    xx = np.column_stack([x[:-1],x[1:]])
    yy = np.column_stack([y[:-1],y[1:]])

    xx0 = np.column_stack([np.full((x.shape[0]),head_[0]),np.full((x.shape[0]),head_[0]+5)])
    yy0 = np.column_stack([np.full((y.shape[0]),head_[1]),np.full((y.shape[0]),head_[1])])
    
    lines_xxyy = np.column_stack([xx,yy])
    lines_xxyy00 = np.column_stack([xx0,yy0])
    
    line_segs1 = np.column_stack([lines_xxyy[:-1],lines_xxyy[1:]])
    line_segs0 = np.column_stack([lines_xxyy00[:-1],lines_xxyy])
    
    line_segs = np.vstack([line_segs0,line_segs1])

    ang_all = np.apply_along_axis(ang,1,line_segs)
    
    return ang_all  


def find_centered_angle(A,B,O):
    AhAB = math.atan2((B.y-A.y), (B.x-A.x)) # -π < AhAB ≤ +π
    AhAO = math.atan2((O.y-A.y), (O.x-A.x)) # -π < AhA) ≤ +π
    AB = AhAO + AhAB                     # -2π < AB ≤ +2π
    #AB = AB + ( 2*math.pi if AB < math.pi else (-2*math.pi if AB> math.pi else 0))
    deg_AB = math.degrees(AB)
    if deg_AB <0:
        deg_AB = deg_AB + 180
    #if deg_AB >270:
        #deg_AB = 360-deg_AB
    return deg_AB

def pointize(pointA,pointB,pointC):
    
    return Point(0,0),Point(pointB[1]-pointA[1],pointB[0]-pointA[0]),Point(pointC[1]-pointA[1],pointC[0]-pointA[0])

def ang_new(x,y):
    
    head_segment = [[x[0:3].mean(),y[0:3].mean()],[x[3:6].mean(),y[3:6].mean()]]
    mid_segment = [[x[7:10].mean(),y[7:10].mean()],[x[10:13].mean(),y[10:13].mean()]]
    tail_segment = [[x[14:17].mean(),y[14:17].mean()],[x[18:20].mean(),y[18:20].mean()]]
    
    ptO,ptB,ptA = pointize([head_segment[0][0],head_segment[0][1]],
                           [mid_segment[1][0],mid_segment[1][1]],
                           [tail_segment[1][0],tail_segment[1][1]])
    ang_deg_BC = find_centered_angle(ptA,ptB,ptO)
    
    ptO,ptB,ptA = pointize([head_segment[0][0],head_segment[0][1]],
                           [mid_segment[0][0],mid_segment[0][1]],
                           [mid_segment[1][0],mid_segment[1][1]])
    ang_deg_AB = find_centered_angle(ptA,ptB,ptO)
    
    ptO,ptB,ptA = pointize([head_segment[0][0],head_segment[0][1]],
                           [tail_segment[0][0],tail_segment[0][1]],
                           [tail_segment[1][0],tail_segment[1][1]])
    ang_deg_AC = find_centered_angle(ptA,ptB,ptO)
    
    
    return ang_deg_AB,ang_deg_AC,ang_deg_BC   
