import random
import cv2
from meenkando.GeneratePlots import PlotFunctions
from PIL import Image
from skimage import *


def sample_skeltonize(dir_path, sorted_files, time_, low_thres=150, high_thresh=220):

    rand_idx = random.randint(1, len(sorted_files))
    imgfile = (dir_path + sorted_files[rand_idx])
    fish_img = cv2.imread(imgfile)
    gray_fish = cv2.cvtColor(fish_img, cv2.COLOR_RGB2GRAY)
    im = cv2.fastNlMeansDenoising(gray_fish, None, 6, 1, 3)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 2))
    closing = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)
    fish_skelton = extract_skelton(closing, 5, low_thres, high_thresh)

    img_fish = Image.fromarray(fish_skelton, 'L')
    bool_fish = img_as_bool(img_fish)

    # do some closing (noise removing)
    d = morphology.disk(10)
    morph_bool_fish = morphology.binary_closing(bool_fish, selem=d)

    # do the skeletonization
    imgSk = morphology.medial_axis(morph_bool_fish)
    imgSk_closing = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)

    PlotFunctions.sample_skeltonize_plots(
        closing, fish_skelton, bool_fish, imgSk, time_[rand_idx])

    return None


def loadImage(f):
    return skimage.img_as_float(skimage.io.imread(f))


def extract_skelton(fish_image, kernel_size, low_threshhold, high_threshhold):
    '''
    Extracts the skeleton from an image of a fish using the Canny edge detector.
    The first step is Gaussian blurring, which reduces noise and makes the result
    more robust to slight changes in threshold values.

    Arguments:
        fish_image: The image of the fish to be skeletonized
        kernel_size: The size of the kernel used in the Gaussian blur
        low_threshhold: The lower threshold for the Canny edge detector
        high_threshhold: The upper threshold for the Canny edge detector

    Returns:
        The skeleton of the fish in the input image
    '''
    blur_gray = cv2.GaussianBlur(fish_image, (kernel_size, kernel_size), 0)
    skelton = cv2.Canny(blur_gray, low_threshhold, high_threshhold)
    skelton[2:5, 0:50] = 0  # mask timestamp
    return skelton
