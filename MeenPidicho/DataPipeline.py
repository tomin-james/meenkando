import numpy as np
import glob
import cv2
import torch.nn as nn
import kornia as K
from kornia.augmentation import AugmentationSequential
import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple
from meenkando.FilePicker import filepicker
from meenkando.NetworkPath import SplineNetwork
from meenkando.MeenPidicho.DataUtilities import vector_to_heatmaps
from meenkando.MeenPidicho.DataUtilities import MODEL_IMG_SIZE


class MeenData(Dataset):
    """
    Class to load comet data from the train_data folder.
    Lots of proprocessing steps needs to be done.

    """

    def __init__(self, config, file_paths, set_type="train"):
        '''
        Defines the data access part for the training pipeline
        '''
        self.device = config["device"]
        self.file_paths = file_paths
        self.mode = set_type

    def __len__(self):
        return len(self.file_paths)

    def __getitem__(self, idx):

        image_paths = self.file_paths[idx]

        if self.mode == 'train':
            img_, mask_, skelton_coords, head_coords = SplineNetwork.extract_fish_splines(
                image_paths, 5, 220)
            img_ = 255-img_
            img_tensor = K.utils.image_to_tensor(img_, keepdim=False)
            img_min_max = K.enhance.normalize_min_max(img_tensor, eps=1e-4)
            img_c = K.color.grayscale_to_rgb(img_min_max)
            coords_ = np.vstack((head_coords[::-1], skelton_coords[np.linspace(
                0, len(skelton_coords)-1, num=10).astype('int')]))

            heatmaps = vector_to_heatmaps(coords_/MODEL_IMG_SIZE)
            #keypoints = torch.tensor(coords_/MODEL_IMG_SIZE)
            heatmaps = torch.from_numpy(np.float32(heatmaps))
            mask = torch.from_numpy(np.float32(mask_))

            aug_list = AugmentationSequential(
                K.augmentation.RandomVerticalFlip(p=0.5),
                K.augmentation.RandomHorizontalFlip(p=0.5),
                K.augmentation.RandomRotation(degrees=90, p=0.5),
                data_keys=['input', 'mask', 'mask'],
                return_transform=False,
                same_on_batch=False,
            )
            out_tensor = aug_list(img_c, mask, heatmaps)

            return {
                "image": torch.squeeze(out_tensor[0]),
                'mask': torch.squeeze(out_tensor[1])[None, :],
                "heatmaps": torch.squeeze(out_tensor[2])
            }

        if self.mode == 'test':
            img = cv2.imread(image_paths)
            gray_fish = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            im = cv2.fastNlMeansDenoising(gray_fish, None, 6, 1, 3)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            closing = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)
            resized = cv2.resize(closing, (128, 128),
                                 interpolation=cv2.INTER_AREA)
            img_tensor = K.utils.image_to_tensor(resized, keepdim=False)
            img_min_max = K.enhance.normalize_min_max(img_tensor, eps=1e-4)
            img_c = K.color.grayscale_to_rgb(img_min_max)

            return torch.squeeze(img_c)
