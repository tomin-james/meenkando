import torch
import torch.nn as nn

from meenkando.MeenPidicho.DataUtilities import MODEL_NEURONS


class ConvBlock(nn.Module):
    '''
    ConvBlock
    =========

    This is a description of `ConvBlock`.

    Args:
        in_depth (int):  Number of input channels.
        out_depth (int): Number of output channels.

    Returns:
        torch.Tensor: The output of the convolution.
    '''

    def __init__(self, in_depth, out_depth):
        super().__init__()
        self.double_conv = nn.Sequential(
            nn.BatchNorm2d(in_depth),
            nn.Conv2d(in_depth, out_depth, kernel_size=3,
                      padding=1, bias=False),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(out_depth),
            nn.Conv2d(out_depth, out_depth, kernel_size=3,
                      padding=1, bias=False),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.double_conv(x)


class ShallowUNet(nn.Module):
    """
    Implementation of UNet, slightly modified:
    - less downsampling blocks
    - less neurons in the layers
    - Batch Normalization added

    Link to paper on original UNet:
    https://arxiv.org/abs/1505.04597
    """

    def __init__(self, in_channel, out_channel):
        super().__init__()

        self.conv_down1 = ConvBlock(in_channel, MODEL_NEURONS)
        self.conv_down2 = ConvBlock(MODEL_NEURONS, MODEL_NEURONS * 2)
        self.conv_down3 = ConvBlock(MODEL_NEURONS * 2, MODEL_NEURONS * 4)
        self.conv_bottleneck = ConvBlock(MODEL_NEURONS * 4, MODEL_NEURONS * 8)

        self.maxpool = nn.MaxPool2d(2)
        self.upsamle = nn.Upsample(
            scale_factor=2, mode="bilinear", align_corners=False)

        self.conv_up1 = ConvBlock(
            MODEL_NEURONS * 8 + MODEL_NEURONS * 4, MODEL_NEURONS * 4
        )
        self.conv_up2 = ConvBlock(
            MODEL_NEURONS * 4 + MODEL_NEURONS * 2, MODEL_NEURONS * 2
        )
        self.conv_up3 = ConvBlock(
            MODEL_NEURONS * 2 + MODEL_NEURONS, MODEL_NEURONS)

        self.conv_out1 = nn.Sequential(
            nn.Conv2d(MODEL_NEURONS, 1,
                      kernel_size=1, stride=1, padding=0),
            nn.Sigmoid(),
        )

        self.conv_out2 = nn.Sequential(
            nn.Conv2d(MODEL_NEURONS, out_channel,
                      kernel_size=3, padding=1, bias=False),
            nn.Sigmoid(),
        )

    def forward(self, x):
        conv_d1 = self.conv_down1(x)
        conv_d2 = self.conv_down2(self.maxpool(conv_d1))
        conv_d3 = self.conv_down3(self.maxpool(conv_d2))
        conv_b = self.conv_bottleneck(self.maxpool(conv_d3))

        conv_u1 = self.conv_up1(
            torch.cat([self.upsamle(conv_b), conv_d3], dim=1))
        conv_u2 = self.conv_up2(
            torch.cat([self.upsamle(conv_u1), conv_d2], dim=1))
        conv_u3 = self.conv_up3(
            torch.cat([self.upsamle(conv_u2), conv_d1], dim=1))

        out1 = self.conv_out1(conv_u3)

        out2 = self.conv_out2(conv_u3)
        return out1, out2


class DoubleShallowUNet(nn.Module):
    """
    Implementation of UNet, slightly modified:
    - less downsampling blocks
    - less neurons in the layers
    - Batch Normalization added

    Link to paper on original UNet:
    https://arxiv.org/abs/1505.04597
    """

    def __init__(self, in_channel, out_channel):
        super().__init__()

        self.conv_down1 = ConvBlock(in_channel, MODEL_NEURONS)
        self.conv_down2 = ConvBlock(MODEL_NEURONS, MODEL_NEURONS * 2)
        self.conv_down3 = ConvBlock(MODEL_NEURONS * 2, MODEL_NEURONS * 4)
        self.conv_bottleneck = ConvBlock(MODEL_NEURONS * 4, MODEL_NEURONS * 8)

        self.maxpool = nn.MaxPool2d(2)
        self.upsamle = nn.Upsample(
            scale_factor=2, mode="bilinear", align_corners=False)

        self.conv_up1 = ConvBlock(
            MODEL_NEURONS * 8 + MODEL_NEURONS * 4, MODEL_NEURONS * 4
        )
        self.conv_up2 = ConvBlock(
            MODEL_NEURONS * 4 + MODEL_NEURONS * 2, MODEL_NEURONS * 2
        )
        self.conv_up3 = ConvBlock(
            MODEL_NEURONS * 2 + MODEL_NEURONS, MODEL_NEURONS)

        self.conv_out1 = nn.Sequential(
            nn.Conv2d(MODEL_NEURONS, 1,
                      kernel_size=1, stride=1, padding=0),
            nn.Sigmoid(),
        )

        self.conv_down1_ = ConvBlock(1, MODEL_NEURONS)
        self.conv_down2_ = ConvBlock(
            MODEL_NEURONS + MODEL_NEURONS*2, MODEL_NEURONS * 2)
        self.conv_down3_ = ConvBlock(
            MODEL_NEURONS * 2 + MODEL_NEURONS*4, MODEL_NEURONS * 4)
        self.conv_bottleneck_ = ConvBlock(
            MODEL_NEURONS * 4, MODEL_NEURONS * 8)

        self.maxpool_ = nn.MaxPool2d(2)
        self.upsamle_ = nn.Upsample(
            scale_factor=2, mode="bilinear", align_corners=False)

        self.conv_up1_ = ConvBlock(
            MODEL_NEURONS * 8 + MODEL_NEURONS * 4, MODEL_NEURONS * 4
        )
        self.conv_up2_ = ConvBlock(
            MODEL_NEURONS * 4 + MODEL_NEURONS * 2, MODEL_NEURONS * 2
        )
        self.conv_up3_ = ConvBlock(
            MODEL_NEURONS * 2 + MODEL_NEURONS, MODEL_NEURONS)

        self.conv_out2 = nn.Sequential(
            nn.Conv2d(MODEL_NEURONS, out_channel,
                      kernel_size=3, padding=1, bias=False),
            nn.Sigmoid(),
        )

    def forward(self, x):
        conv_d1 = self.conv_down1(x)
        conv_d2 = self.conv_down2(self.maxpool(conv_d1))
        conv_d3 = self.conv_down3(self.maxpool(conv_d2))
        conv_b = self.conv_bottleneck(self.maxpool(conv_d3))

        conv_u1 = self.conv_up1(
            torch.cat([self.upsamle_(conv_b), conv_d3], dim=1))
        conv_u2 = self.conv_up2(
            torch.cat([self.upsamle_(conv_u1), conv_d2], dim=1))
        conv_u3 = self.conv_up3(
            torch.cat([self.upsamle_(conv_u2), conv_d1], dim=1))

        out1 = self.conv_out1(conv_u3)

        conv_d1_ = self.conv_down1_(out1)
        conv_d2_ = self.conv_down2_(
            torch.cat([self.maxpool_(conv_d1_), conv_u2], dim=1))
        conv_d3_ = self.conv_down3_(
            torch.cat([self.maxpool_(conv_d2_), conv_u1], dim=1))
        conv_b_ = self.conv_bottleneck_(self.maxpool_(conv_d3_))

        conv_u1_ = self.conv_up1_(
            torch.cat([self.upsamle_(conv_b_), conv_d3_], dim=1))
        conv_u2_ = self.conv_up2_(
            torch.cat([self.upsamle_(conv_u1_), conv_d2_], dim=1))
        conv_u3_ = self.conv_up3_(
            torch.cat([self.upsamle_(conv_u2_), conv_d1_], dim=1))

        out2 = self.conv_out2(conv_u3_)

        return out1, out2
