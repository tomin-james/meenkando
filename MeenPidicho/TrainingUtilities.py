
import torch
import numpy as np
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple
import torch.nn as nn
import kornia as K
from tqdm import tqdm
import matplotlib.pyplot as plt
from meenkando.MeenPidicho.DataUtilities import MODEL_IMG_SIZE, heatmaps_to_coordinates


class IoULoss(nn.Module):
    """
    Intersection over Union Loss.
    IoU = Area of Overlap / Area of Union
    IoU loss is modified to use for heatmaps.
    """

    def __init__(self):
        super(IoULoss, self).__init__()
        self.EPSILON = 1e-6

    def _op_sum(self, x):
        return x.sum(-1).sum(-1)

    def forward(self, y_pred, y_true):
        inter = self._op_sum(y_true * y_pred)
        union = (
            self._op_sum(y_true ** 2)
            + self._op_sum(y_pred ** 2)
            - self._op_sum(y_true * y_pred)
        )
        iou = (inter + self.EPSILON) / (union + self.EPSILON)
        iou = torch.mean(iou)
        return 1 - iou


def show_data(dataset, n_samples=12):
    """
    Function to visualize data
    Input: torch.utils.data.Dataset
    """
    n_cols = 4
    n_rows = int(np.ceil(n_samples / n_cols))
    fig, ax = plt.subplots(
        n_rows*2, n_cols, figsize=(15, n_rows*4))

    img_arr = np.empty((MODEL_IMG_SIZE, MODEL_IMG_SIZE, n_samples))

    ids = np.random.choice(dataset.__len__(), n_samples, replace=False)
    for i, id_ in enumerate(tqdm(ids)):
        sample = dataset.__getitem__(id_)
        image = K.utils.tensor_to_image(sample["image"], keepdim=False)
        mask = K.utils.tensor_to_image(sample["mask"], keepdim=False)
        img_arr[:, :, i] = image[:, :, 0]
        keypoints = heatmaps_to_coordinates(sample['heatmaps'])

        row_ = i // n_cols
        col_ = i % n_cols

        ax[row_, col_].imshow(image)
        ax[row_, col_].scatter(
            keypoints[:, 0], keypoints[:, 1], c="r", alpha=0.5)
        ax[row_+2, col_].imshow(mask)

    plt.tight_layout()
    plt.show()

    return img_arr


def show_batch_predictions(batch_data, model):
    """
    Visualizes image, image with actual keypoints and
    image with predicted keypoints.
    Finger colors are in COLORMAP.
    Inputs:
    - batch data is batch from dataloader
    - model is trained model
    """
    inputs = batch_data["image"]
    true_keypoints = batch_data["keypoints"].numpy()
    batch_size = true_keypoints.shape[0]
    pred_heatmaps = model(inputs)
    pred_heatmaps = pred_heatmaps.detach().numpy()
    pred_keypoints = heatmaps_to_coordinates(pred_heatmaps)
    images = batch_data["image_raw"].numpy()
    images = np.moveaxis(images, 1, -1)

    plt.figure(figsize=[12, 4 * batch_size])
    for i in range(batch_size):
        image = images[i]
        true_keypoints_img = true_keypoints[i] * RAW_IMG_SIZE
        pred_keypoints_img = pred_keypoints[i] * RAW_IMG_SIZE

        plt.subplot(batch_size, 3, i * 3 + 1)
        plt.imshow(image, cmap='gray')
        plt.title("Image")
        plt.axis("off")

        plt.subplot(batch_size, 3, i * 3 + 2)
        plt.imshow(image)
        for finger, params in COLORMAP.items():
            plt.plot(
                true_keypoints_img[params["ids"], 0],
                true_keypoints_img[params["ids"], 1],
                params["color"],
            )
        plt.title("True Keypoints")
        plt.axis("off")

        plt.subplot(batch_size, 3, i * 3 + 3)
        plt.imshow(image)
        for finger, params in COLORMAP.items():
            plt.plot(
                pred_keypoints_img[params["ids"], 0],
                pred_keypoints_img[params["ids"], 1],
                params["color"],
            )
        plt.title("Pred Keypoints")
        plt.axis("off")
    plt.tight_layout()
    plt.axis("off")
