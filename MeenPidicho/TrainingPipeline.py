import torch
import numpy as np
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple
import torch.nn as nn
import kornia as K
from tqdm import tqdm


class Trainer:
    def __init__(self, model, criterion, optimizer, config, scheduler=None):
        self.model = model
        self.criterion = criterion
        self.optimizer = optimizer
        self.loss = {"train": [], "val": [],
                     'train_loss1': [], 'train_loss2': []}
        self.epochs = config["epochs"]
        self.batches_per_epoch = config["batches_per_epoch"]
        self.batches_per_epoch_val = config["batches_per_epoch_val"]
        self.device = config["device"]
        self.scheduler = scheduler
        self.checkpoint_frequency = 2
        self.early_stopping_epochs = 10
        self.early_stopping_avg = 10
        self.early_stopping_precision = 5
        self.source_folder = config['source_folder']
        self.model_name = config['model_name']

    def train(self, train_dataloader, val_dataloader):
        for epoch in tqdm(range(self.epochs)):
            self._epoch_train(train_dataloader)
            self._epoch_eval(val_dataloader)
            print(
                "Epoch: {}/{}, Train Loss={}, Val Loss={}".format(
                    epoch + 1,
                    self.epochs,
                    np.round(self.loss["train"][-1], 10),
                    np.round(self.loss["val"][-1], 10),
                )
            )

            print(
                "Epoch: {}/{}, Train out1 Loss={}, Train out2 Loss={}".format(
                    epoch + 1,
                    self.epochs,
                    np.round(self.loss["train_loss1"][-1], 10),
                    np.round(self.loss["train_loss2"][-1], 10),
                )
            )

            # reducing LR if no improvement
            if self.scheduler is not None:
                self.scheduler.step(self.loss["train"][-1])

            # saving model
            if (epoch + 1) % self.checkpoint_frequency == 0:
                torch.save(
                    self.model.state_dict(
                    ), self.source_folder+self.model_name+"model_{}".format(str(epoch + 1).zfill(3))
                )

            # early stopping
            if epoch < self.early_stopping_avg:
                min_val_loss = np.round(
                    np.mean(self.loss["val"]), self.early_stopping_precision)
                no_decrease_epochs = 0

            else:
                val_loss = np.round(
                    np.mean(self.loss["val"][-self.early_stopping_avg:]),
                    self.early_stopping_precision
                )
                if val_loss >= min_val_loss:
                    no_decrease_epochs += 1
                else:
                    min_val_loss = val_loss
                    no_decrease_epochs = 0
                    # print('New min: ', min_val_loss)

            if no_decrease_epochs > self.early_stopping_epochs:
                print("Early Stopping")
                break

        torch.save(self.model.state_dict(), "model_final")
        return self.model

    def _epoch_train(self, dataloader):
        self.model.train()
        running_loss = []
        running_loss1 = []
        running_loss2 = []

        for i, data in enumerate(dataloader, 0):
            inputs = data["image"].float().to(self.device)
            segment_mask = data['mask'].to(self.device)
            keypoint_labels = data["heatmaps"].to(self.device)

            self.optimizer.zero_grad()

            outputs1, outputs2 = self.model(inputs)
            loss1 = self.criterion(outputs1, segment_mask)
            loss2 = self.criterion(outputs2, keypoint_labels)
            loss = loss1+loss2

            if (i > 0) and (i % 4 == 0):
                loss.backward(retain_graph=True)

            loss2.backward()
            self.optimizer.step()

            running_loss.append(loss.item())
            running_loss1.append(loss1.item())
            running_loss2.append(loss2.item())

            if i == self.batches_per_epoch:
                epoch_loss = np.mean(running_loss)
                epoch_loss1 = np.mean(running_loss1)
                epoch_loss2 = np.mean(running_loss2)
                self.loss["train"].append(epoch_loss)
                self.loss['train_loss1'].append(epoch_loss1)
                self.loss['train_loss2'].append(epoch_loss2)
                break

    def _epoch_eval(self, dataloader):
        self.model.eval()
        running_loss = []

        with torch.no_grad():
            for i, data in enumerate(dataloader, 0):
                inputs = data["image"].float().to(self.device)
                segment_mask = data['mask'].to(self.device)
                keypoint_labels = data["heatmaps"].to(self.device)

                outputs1, outputs2 = self.model(inputs)
                loss1 = self.criterion(outputs1, segment_mask)
                loss2 = self.criterion(outputs2, keypoint_labels)
                loss = loss1+loss2

                running_loss.append(loss.item())

                if i == self.batches_per_epoch_val:
                    epoch_loss = np.mean(running_loss)
                    self.loss["val"].append(epoch_loss)
                    break
