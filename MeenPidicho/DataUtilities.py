import numpy as np
import os
import glob
from PIL import Image
from astropy.io import fits
import cv2
import matplotlib.pyplot as plt
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple

N_KEYPOINTS = 11
N_IMG_CHANNELS = 3
MODEL_IMG_SIZE = 128
MODEL_NEURONS = 16


def create_train_test_split(L: int, ratio: float = 0.8) -> npt.ArrayLike:
    '''
    Takes in as input the number of folders available
    as training comets and the required ratio of the split.
    Outputs two arrays containing the indexes for train and validatioin 
    splits of the folders to be used.
    '''

    perm_order = np.random.permutation(L)
    train_ = perm_order[:int(ratio*L)]
    val_ = perm_order[int(ratio*L):]
    return train_, val_


def normalize_image_vectorise(img_arr: npt.NDArray, smax: float) -> npt.NDArray:
    '''
    A simple utility function to enable normalizing of an image
    between 0 and a user defined upper level.
    If possible always use K.minmax from Kornia
    '''
    smin = 0.0
    img_min, img_max = img_arr.min(axis=(0, 1)), img_arr.max(axis=(0, 1))
    img_arr = (((img_arr - img_min) * (smax - smin)) /
               (img_max - img_min)) + smin
    return img_arr


def bck_sub_normalize(flux: npt.ArrayLike, flux_lim: float) -> npt.ArrayLike:
    '''
    An iterative flux removal utility to remove RFI noises.
    The flux substraction is done on the frequency axis. 
    '''
    start_mean = flux.mean()
    while True:
        temp_Data_I = flux - np.mean(flux, axis=0)
        flux = temp_Data_I.clip(min=0)
        if (((start_mean - flux.mean())/start_mean) >= flux_lim) & (flux.mean() > flux_lim):
            start_mean = flux.mean()
        else:
            break
    flux = ((flux - flux.min())/(flux.max()-flux.min()))*255
    return flux


def vector_to_heatmaps(keypoints: npt.ArrayLike) -> npt.NDArray:
    """
    Creates 2D heatmaps from keypoint locations for a single image
    Input: array of size N_KEYPOINTS x 2
    Output: array of size N_KEYPOINTS x MODEL_IMG_SIZE x MODEL_IMG_SIZE
    """
    heatmaps = np.zeros([N_KEYPOINTS, MODEL_IMG_SIZE, MODEL_IMG_SIZE])
    for k, (x, y) in enumerate(keypoints):
        x, y = int(x * MODEL_IMG_SIZE), int(y * MODEL_IMG_SIZE)
        if (0 <= x < MODEL_IMG_SIZE) and (0 <= y < MODEL_IMG_SIZE):
            heatmaps[k, int(x), int(y)] = 1

    heatmaps = blur_heatmaps(heatmaps)
    return heatmaps


def blur_heatmaps(heatmaps: npt.ArrayLike):
    """Blurs heatmaps using GaussinaBlur of defined size"""
    heatmaps_blurred = heatmaps.copy()
    for k in range(len(heatmaps)):
        if heatmaps_blurred[k].max() == 1:
            heatmaps_blurred[k] = cv2.GaussianBlur(heatmaps[k], (3, 3), 3)
            heatmaps_blurred[k] = heatmaps_blurred[k] / \
                heatmaps_blurred[k].max()
    return heatmaps_blurred


def heatmaps_to_coordinates(heatmaps: npt.NDArray) -> npt.ArrayLike:
    """
    Heatmaps is a numpy array
    Its size - (batch_size, n_keypoints, img_size, img_size)
    """
    sums = heatmaps.sum(axis=-1).sum(axis=-1)
    sums = np.expand_dims(sums, [1, 2])
    normalized = heatmaps / sums
    normalized = normalized.numpy()
    x_prob = normalized.sum(axis=1)
    y_prob = normalized.sum(axis=2)

    arr = np.tile(np.float32(np.arange(0, MODEL_IMG_SIZE)), [N_KEYPOINTS, 1])
    x = (arr * x_prob).sum(axis=1)
    y = (arr * y_prob).sum(axis=1)
    keypoints = np.stack([x, y], axis=-1)
    return keypoints
