# MeenKando

A computer vision software to track movements of zebrafish larvae for neuroscience behaviour experiments.
It also includes a workflow to train a deep learning model in a completely unsupervised manner to estimate 
pose and for image segmentation. No manual labels are given for training the deep learning model.
The necessary labels are automatically determined during the training workflow.
The data is in the form of BMP images recorded by a high speed camery usually at 1 ms latency. 
The data acquistion procedure and the experiments are detailed in Nagar 2021.

# Description

MEENKANDO ( coming from the native malayalam language - meaning 'did you see the fish'), contains two major 
functionalities - time stamp decoding and pose estimation.
In time stamp decoding, the program converts the 8 bit coded pixels in the top left of the image into the time
of the image acquistion. The start time of the image acquistion experiment could be ascertained from the 
image in the sequence. For pose estimation the program uses a traditional computer vision approach, instead of deep learning.
The algorithm achieves this by extracting the skelton of the fish through morphological contour operations and then 
fitting the skelton using a directed graph with the help of Djikstra's path finding algorithm. 

We have also extended the program as a way to generate automatic labels needed to train a deep learning model.
In this way we are able to do an end to end unsupervised way of estimating the pose.

![alt text](imgs/1.png "Extracting timestamp")

The above figure shows the timeline of the experiment, with the the tap position. 
A 'tap' is defined as the startling impulse given to the fish, to measure its response to stimulus.
The number of taps and the interval between them vary in experiments. The resulting time line is then used to measure,
the response of the fish as a function of time.

![alt text](imgs/2.png "Skeltonizing fish")

The fish is skeltonized usinga contour thresholding method. From the extracted outline of the fish, 
we do morphological closing using a rectangular kernel.

![Escape response](imgs/WT_Escape_MeenKando.mp4)

( if the movie is not working,it could be viewed directly from [here](/imgs/WT_Escape_MeenKando.mp4))

The movie shows the full tracking of the fish for a single instance of the stimuli.
The blue line is the fitted graph, with the red dots the nodes. The number of nodes
can be inputed as a configurable option. 

![alt text](imgs/3.png "Extracting timestamp")

From the extracted graph nodes, we can construct any measure to track the movement of the fish.
Here, for example we have created a graph that measures the relative angles between different body components.
This is useful to measure the latency in response to a stimuli.


## Installation
The module can be run as it is. However the data reading functions are tuned only for the particular format of the images
as obtained from a high speed camera.

## Roadmap
Currently, we are planning to extend it other organisms like worms.
The agenda is to create pipelines for automatic pose estimations for biological experiments 
in fully input-free manner.


## Contributing
I would love if you could contribute more to this project.
You can contact me more details via [email](mailto:tomin.james@outlook.com?subject=Meenkando-contribution)

## Authors and acknowledgment
This software would not have been possible without Dr Dhriti Nagar's active support and guidance.
All the data needed for the software resulted out of her sleepless toil.


## License
The project is licensed under MIT.

## Project status
The project is being acively developed. So do contact if you have more ideas.
