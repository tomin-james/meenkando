import os


def extract_files(data_dir, fish_dir, file_type='.BMP', dot_pos=1, uscore_pos=4):
    '''
    The function will look for all images in the directory, and sort them in alphabetical order.

    The output of the function is a list of image file names.

    Arguments:
    - data_dir: the path to the root folder of the data
    - fish_dir: a folder containing the images, usually 'ALL'
    - file_type: the file type of the images, usually '.BMP'
    - dot_pos: the position of the dot before the file type extension, typically 1 for '.BMP'
    - uscore_pos: the position of the underscore before the year information

    Example:
        extract_files('/home/test')
    '''

    image_dir = os.listdir(data_dir+fish_dir)
    img_files = [file_i
                 for file_i in image_dir
                 if file_i.endswith(file_type)]
    print("Found %2d images" % (len(img_files)))
    #sorted_files = sorted(img_files, key=lambda x: int((x.split('.')[dot_pos]).split('_')[uscore_pos]))
    sorted_files = sorted(img_files)
    return sorted_files
