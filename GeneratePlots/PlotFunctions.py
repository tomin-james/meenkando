import matplotlib.pyplot as plt
from IPython.display import display, Image,clear_output
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib.patches import Circle
from skimage.morphology import skeletonize
from skimage.util import invert
from skimage import *
import networkx as nx
from PIL import Image
import numpy as np
from meenkando.utils import FindPeaks


def time_stamp_plots(time_,tap_pix_idx):
    
    fig,axes = plt.subplots(1,2,figsize=(12,5), sharex=False, sharey=False, facecolor='w')
    
    axes[0,].plot(time_)
    axes[1,].plot(time_,tap_pix_idx)
    axes[0,].set_title("Time from the images vs fig numbers")
    axes[1,].set_title("Tap positions as a function of time")
    axes[0,].set_xlabel('Fig numbers')
    axes[0,].set_ylabel('Time (secs)')
    axes[1,].set_xlabel('Time (secs)')
    
    
    fig.suptitle("Time from the images and tap positions", fontsize=16)
    plt.show()
    
    return None

def sample_skeltonize_plots(img1,img2,img3,img4,time_in_frame):
    fig,axes = plt.subplots(1,4,figsize=(15,6), sharex=False, sharey=False,  facecolor='w')
    
    axes[0,].imshow(img1, cmap='gray')
    axes[0,].text(100, 5, str(round(time_in_frame,3)), horizontalalignment='center',
         verticalalignment='center',fontsize=16)
    axes[1,].imshow(img2, cmap='gray')
    axes[2,].imshow(img3, cmap='gray', interpolation="nearest")
    axes[3,].imshow(img4, cmap='gray', interpolation="nearest")
    
    axes[0,].set_title('Raw image')
    axes[1,].set_title('Extracted skelton')
    axes[2,].set_title('Bool image')
    axes[3,].set_title('Morphological median axis')
    
    fig.suptitle("Skeltonizing process ", fontsize=16)
    plt.show()
    
    return None
    
class PosWrapper(dict):
     def __getitem__(self,key):
        return [key.point[1], key.point[0]] # switch x and y
    
def create_fish_film_plots(time_in_frame,idx_no,gray,skelton,bool_fish,graph2,nd2,tap_index,de_str):
    
    fig,axes = plt.subplots(1,4,figsize=(12,8), sharex=False, sharey=True,facecolor='w')


    axes[0,].imshow(gray, cmap=cm.gray,interpolation='nearest')
    axes[0,].text(100, 5, str(round(time_in_frame,3)), horizontalalignment='center',
           verticalalignment='center',fontsize=16)

    if tap_index:
        circ = Circle((20,20),5)
        axes[0,].add_patch(circ) 

    axes[1,].imshow(skelton, cmap=cm.gray)
    axes[2,].imshow(bool_fish, cmap=cm.gray)
    nx.draw_networkx(graph2,PosWrapper(), ax=axes[2,], 
                     with_labels=False, 
                     node_color= 'b', 
                     edge_color='b' , alpha=0.5,node_size=5
                     )
    axes[3,].imshow(gray, cmap=cm.gray)
    nx.draw_networkx(graph2,PosWrapper(), ax=axes[3,], 
                     with_labels=False, 
                     node_color= 'c', 
                     edge_color='c' , node_size=15
                     )
    nx.draw_networkx(graph2,nodelist = nd2[0::10], pos=PosWrapper(), ax=axes[3,], 
                     with_labels=False, 
                     node_color="r", 
                     edge_color='r' , node_size=10
                     )

    plt.savefig('fish_film/'+de_str+'fish_film_'+str(idx_no)+'.png',dpi='figure',format='png')
    plt.close()
    
    return None

def calc_plot_latency(start_,end_,angles_,cutoff_fac,time_,tap_no,de_str,first_mov=5):
    
    fig,axes = plt.subplots(1,3,figsize=(15,6), sharex=False, sharey=False,facecolor='w')
    
    
    tap_mark = start_
    marker_line = np.linspace(0, 200, 100)
    tap_time = 100 * [tap_mark]
    angles_abs = angles_ + 500
    
    for angle_id in range(3):
        pre_max = np.max(angles_abs[70:90,angle_id])
        pre_min = np.min(angles_abs[70:90,angle_id])
        axes[angle_id,].plot(time_[50:250],
                              angles_[50:250,angle_id],'.-')
        try:
            cutoff_high = np.where(angles_abs[100:140,angle_id] >= pre_max+cutoff_fac)[0][0] + 100
        except:
            cutoff_high = 150
        try:
            cutoff_low = np.where(angles_abs[100:140,angle_id] <= pre_max-cutoff_fac)[0][0] + 100
        except:
            cutoff_low = 150
        latency = np.min([time_[cutoff_high]-tap_mark,time_[cutoff_low]-tap_mark])
        
        axes[angle_id,].plot(tap_time,marker_line,linewidth=3,color='r',linestyle='--',label='Tap')
        
        try:
            c_turn_cutoff_high = np.where(angles_abs[100:140,angle_id] > pre_max+first_mov)[0][0] + 100
        except:
            c_turn_cutoff_high = 200
        try:
            c_turn_cutoff_low = np.where(angles_abs[100:140,angle_id] < pre_min-first_mov)[0][0] + 100
        except:
            c_turn_cutoff_low = 200
        c_latency = np.min([time_[c_turn_cutoff_high]-tap_mark,time_[c_turn_cutoff_low]-tap_mark])
        axes[angle_id,].text(0.4,0.9,'First movement = '+ str(round(c_latency,5)*1000)+' ms', 
                             transform=axes[angle_id,].transAxes)
        
        axes[angle_id,].text(0.4,0.8,'Latency cutoff = '+ str(round(latency,5)*1000)+' ms', 
                             transform=axes[angle_id,].transAxes)
        axes[angle_id,].set_ylim([np.min(angles_[50:250,angle_id])-10,
                  np.max(angles_[50:250,angle_id])+10])
        axes[angle_id,].set_xlabel('Time(secs)')
        axes[angle_id,].set_ylabel('Angle(°)')
        
    axes[0,].set_title('Angle between Head and Body')
    axes[1,].set_title('Angle between Head and Tail')
    axes[2,].set_title('Angle between Body and Tail')
    
    
    fig.suptitle(de_str+" Latency Calculation for Tap no: %d"%(tap_no+1), fontsize=16)
    plt.show()
    
    return None

def calc_plot_latency_interp(start_,end_,angles_,cutoff_fac,time_,tap_no,de_str,first_mov=3.5):
    
    fig,axes = plt.subplots(1,3,figsize=(15,6), sharex=False, sharey=False,facecolor='w')
    
    
    tap_mark = start_
    marker_line = np.linspace(0, 200, 100)
    tap_time = 100 * [tap_mark]
    tap_time_idx = np.where(time_ >= tap_mark)[0][0]
    angles_abs = np.copy(angles_)
    angles_abs[0:tap_time_idx,:] = np.mean(angles_[0:tap_time_idx,:],axis=0)
    latencies = []
    for angle_id in range(3):
        pre_level_high =  np.max(angles_abs[0:tap_time_idx,angle_id])
        pre_level_low  =  np.min(angles_abs[0:tap_time_idx,angle_id])

        new_time = np.linspace(time_[tap_time_idx], time_[tap_time_idx+150], 1000)
        angles_interp_abs = np.interp(new_time, time_[tap_time_idx:tap_time_idx+140],
                                      angles_abs[tap_time_idx:tap_time_idx+140,angle_id])
        
        peak_idxs = FindPeaks.detect_peaks(angles_interp_abs,mph=pre_level_high*4,show=False)
        axes[angle_id,].plot(time_[tap_time_idx-50:tap_time_idx+250],
                              angles_[tap_time_idx-50:tap_time_idx+250,angle_id],'.-')
        axes[angle_id,].plot(new_time,
                              angles_interp_abs,'.')
        axes[angle_id,].scatter(new_time[peak_idxs], angles_interp_abs[peak_idxs],c="g",marker='+')
        try:
            cutoff_high = np.where(angles_interp_abs >= (pre_level_high+cutoff_fac))[0][0] 
        except:
            cutoff_high = 350
        try:
            cutoff_low = np.where(angles_interp_abs <= (pre_level_high-cutoff_fac))[0][0] 
        except:
            cutoff_low = 350
        latency = np.min([new_time[cutoff_high]-tap_mark,new_time[cutoff_low]-tap_mark])
        
        axes[angle_id,].plot(tap_time,marker_line,linewidth=3,color='r',linestyle='--',label='Tap')
        
        try:
            first_mov_high = np.where(angles_interp_abs >= (pre_level_high+first_mov))[0][0]
        except:
            first_mov_high = -999
        #try:
            #first_mov_low = np.where(angles_interp_abs <= (pre_level_low-first_mov))[0][0] 
            #if first_mov_low < 0.5: first_mov_low = np.where(angles_interp_abs <= (pre_level_low-first_mov))[0][1] 
        #except:
        #first_mov_low = 300
        #first_mov_latency = np.min([new_time[first_mov_high]-tap_mark,new_time[first_mov_low]-tap_mark])
        first_mov_latency = new_time[first_mov_high]-tap_mark
        latencies.append(first_mov_latency)
        
        axes[angle_id,].text(0.4,0.9,'First movement = '+ str(np.around(first_mov_latency*1000,decimals=3))+' ms', 
                             transform=axes[angle_id,].transAxes)
        
        axes[angle_id,].text(0.4,0.8,'C bend = '+ str(np.around(latency*1000,decimals=3))+' ms', 
                             transform=axes[angle_id,].transAxes)
        axes[angle_id,].set_ylim([np.min(angles_[50:250,angle_id])-10,
                  np.max(angles_[50:250,angle_id])+10])
        axes[angle_id,].set_xlabel('Time(secs)')
        axes[angle_id,].set_ylabel('Angle(°)')
        
    latencies = np.asarray(latencies)
    latencies = latencies[(latencies >= 0.001) & (latencies <= 0.05)]
    print(latencies)
        
    axes[0,].set_title('Angle between Head and Body')
    axes[1,].set_title('Angle between Head and Tail')
    axes[2,].set_title('Angle between Body and Tail')
    
    axes[1,].text(0.1,1.10,
                  'Avg Latency first movement = '+ str(np.around(np.mean(np.asarray(latencies))*1000,decimals=3))+' ms', 
                             transform=axes[angle_id,].transAxes)
    fig.suptitle(de_str+" Latency Calculation for Tap no: %d"%(tap_no+1), fontsize=16)
    plt.show()
    
    return None

def plot_latency_interp_all(start_,angles_,time_):
    
    tap_mark = start_
    marker_line = np.linspace(0, 200, 100)
    tap_time = 100 * [tap_mark]
    
    fig,axes = plt.subplots(13,3,figsize=(15,6), sharex=False, sharey=False,facecolor='w')
    angle_id = 0
    for ii in range(13):
        for jj in range(3):
            
            axes[ii,jj].plot(tap_time,marker_line,linewidth=3,color='r',linestyle='--',label='Tap')
            axes[ii,jj].plot(time_[50:250],
                                  angles_[50:250,angle_id],'.-')
            angle_id += 1

    return None

def create_fish_film_plots_with_ang(time_in_frame,idx_no,gray,skelton,bool_fish,graph2,nd2,tap_index,x,y,head_coord,
                                    angle,de_str):
    
    fig,axes = plt.subplots(1,5,figsize=(12,8), sharex=False, sharey=True,facecolor='w')


    axes[0,].imshow(gray, cmap=cm.gray,interpolation='nearest')
    axes[0,].text(100, 5, str(round(time_in_frame,3)), horizontalalignment='center',
           verticalalignment='center',fontsize=16)

    if tap_index:
        circ = Circle((20,20),5)
        axes[0,].add_patch(circ) 

    axes[1,].imshow(skelton, cmap=cm.gray)
    axes[2,].imshow(bool_fish, cmap=cm.gray)
    nx.draw_networkx(graph2,PosWrapper(), ax=axes[2,], 
                     with_labels=False, 
                     node_color= 'b', 
                     edge_color='b' , alpha=0.5,node_size=5
                     )
    axes[3,].imshow(gray, cmap=cm.gray)
    nx.draw_networkx(graph2,PosWrapper(), ax=axes[3,], 
                     with_labels=False, 
                     node_color= 'c', 
                     edge_color='c' , node_size=15
                     )
    nx.draw_networkx(graph2,nodelist = nd2[0::10], pos=PosWrapper(), ax=axes[3,], 
                     with_labels=False, 
                     node_color="r", 
                     edge_color='r' , node_size=10
                     )
    axes[4,].imshow(np.zeros_like(gray),cmap=cm.gray)
    axes[4,].plot([head_coord[0],head_coord[0]+10],[head_coord[1],head_coord[1]],'-w',linewidth=5)
    axes[4,].plot([head_coord[0]+10,x[0:3].mean()],[head_coord[1],y[0:3].mean()],'-r',linewidth=4)
    axes[4,].plot([x[4:8].mean(),x[9:13].mean()], [y[4:8].mean(),y[9:13].mean()],'-c',linewidth=4)
    axes[4,].plot([x[13:16].mean(),x[17:20].mean()], [y[13:16].mean(),y[17:20].mean()],'-g',linewidth=4)
    axes[4,].text(0.2, 0.70, 'bt:'+str(round(angle[2],3)), horizontalalignment='left', #body-tail
           verticalalignment='center',color='w',fontsize=10,transform=axes[4,].transAxes)
    axes[4,].text(0.2, 0.80, 'ht:'+str(round(angle[1],2)), horizontalalignment='left', #head-tail
           verticalalignment='center',color='w',fontsize=10,transform=axes[4,].transAxes)
    axes[4,].text(0.2, 0.90, 'h:'+str(round(angle[0],1)), horizontalalignment='left',  #head-angle
           verticalalignment='center',color='w',fontsize=10,transform=axes[4,].transAxes)
    

    plt.savefig('fish_film/'+de_str+'fish_film_with_ang_'+str(idx_no)+'.png',dpi='figure',format='png')
    plt.close()
    
    return None
